using System;
using System.Text;
namespace StringFormatLib
{
    class Program
    {
        //30000000000 to 30,000,000,000
        //2 params: input string, separator
        //Thousand Separator is not using pattern.
        static string ThousandSeparator(string input)
        {
            int srcLength = input.Length;
            int srcIndex = srcLength - 1;
            int resultLength = srcLength + (srcLength - 1) / 3;
            int destIndex = resultLength-1;
            var rs = new char[resultLength];
            while (srcIndex >= 0) {
                rs[destIndex] = input[srcIndex];
                --srcIndex;
                --destIndex;

                if ((srcLength - srcIndex -1) % 3 == 0) {
                    rs[destIndex] = ',';
                    --destIndex;
                }
            }
            var output = string.Join("", rs);
            return output;
        }
        //C# Custom Formats with input is a string
        //For example, i am passing the decimal value is 75674.73789621.
        
        static string formatString(string pattern, string input){         
            // vị trí của dấu . trong patter
            int indexOfPatternDecimalPoint = pattern.IndexOf("."); 
            //vị trí của dấu . trong input
            int indexOfInputDecimalPoint = input.IndexOf("."); 
            //phần sau dấu chấm của input
            int inputRearPart_length = input.Substring(indexOfInputDecimalPoint + 1).Length;
            //pattern sau khi cắt bỏ phần sau dấu . (tính cả dấu .)
            var patternPreviousPart = pattern.Substring(0, indexOfPatternDecimalPoint + 1);
            //phần phía sau dấu . của pattern
            var patternRearPart =  pattern.Substring(indexOfPatternDecimalPoint + 1); 
            //chiều dài chuỗi input
            int inputLength = input.Length;        
            // chiều dài phần phía sau pattern ( dã trừ dấu })
            int patternRearPart_length = patternRearPart.Length - 1; 
            //chiều dài phần giữ lại
            int inputKeepPart = indexOfInputDecimalPoint + patternRearPart_length + 1;  

             /*=================Decimal Point======Zero Placeholder========================
            **Character: 0 or .
            **Usage: {0:0.00} or {0:00.00}
            **Returns:  string
            **Result example: 75674.74
            ==============================================================================*/
             //kiểm tra phần đầu củapattern để xác định format
            if(patternPreviousPart == "{0:00." || patternPreviousPart == "{0:0."){       
                if(inputRearPart_length < patternRearPart_length){
                    Console.WriteLine("Error message here!");
                }
                if(inputRearPart_length == patternRearPart_length){
                    return input;
                }
                else{
                    //kiểm tra vị trí sau số cần làm tròn 1 đơn vị để làm tròn số
                    if(input[indexOfInputDecimalPoint + patternRearPart_length + 1] < '5'){ 
                        input = input.Substring(0, inputKeepPart); // +1 vì vị trí bắt đầu từ 0
                    }
                    else{
                        //kí tự cần làm tròn của input cuối cùng
                        if(input[indexOfInputDecimalPoint + patternRearPart_length ] == '9'){
                            var  lastChar = input.Substring(indexOfInputDecimalPoint + patternRearPart_length - 1, 1);
                            //Console.WriteLine("{0}", lastChar);
                            var lastInt = Int16.Parse(lastChar) + 1;
                            //Console.WriteLine("{0}", lastInt);
                            var str = lastInt.ToString();
                            //Console.WriteLine("string {0}", str);
                            char[] c = str.ToCharArray(); 
                            StringBuilder sb = new StringBuilder(input);
                            sb[indexOfInputDecimalPoint + patternRearPart_length] = '0';
                            sb[indexOfInputDecimalPoint + patternRearPart_length - 1] = c[0];
                            input = sb.ToString();
                            input = input.Substring(0, inputKeepPart);
                        }
                        else{
                            var  lastChar = input.Substring(indexOfInputDecimalPoint + patternRearPart_length, 1);
                            var lastInt = Int16.Parse(lastChar) + 1;
                            //Console.WriteLine("{0}", lastInt);
                            var str = lastInt.ToString();
                            //Console.WriteLine("string {0}", str);
                            char[] c = str.ToCharArray(); 
                            StringBuilder sb = new StringBuilder(input);
                            sb[indexOfInputDecimalPoint + patternRearPart_length] = c[0];
                            input = sb.ToString();
                            input = input.Substring(0, inputKeepPart);
                        }
                    }    
                }       
            }
             /*========================Thousand ThousandSeparator========================
            **Character: 0
            **Usage: {0:0,0} 
            **Returns:  string
            **Result example: 75,675
            ==============================================================================*/
            if(pattern == "{0:0,0}"){ 
                input = input.Substring(0, indexOfInputDecimalPoint);
                input = ThousandSeparator(input);
            }
             /*========================Digit Placeholder========================
            **Character: #
            **Usage: {0:(#).##}
            **Returns:  string
            **Result example: (75674).74
            ==============================================================================*/
            if(patternPreviousPart == "{0:(#)."){ 
                if(inputRearPart_length < patternRearPart_length){
                    Console.WriteLine("Error message here!");
                }
                if(inputRearPart_length == patternRearPart_length){
                    return input;
                }
                else{
                    //kiểm tra vị trí sau số cần làm tròn 1 đơn vị để làm tròn số
                        if(input[indexOfInputDecimalPoint + patternRearPart_length + 1] < '5'){ 
                            input = input.Substring(0, inputKeepPart); // +1 vì vị trí bắt đầu từ 0
                            input = input.Insert(0, "(");
                            input = input.Insert(indexOfInputDecimalPoint + 1, ")");
                        }
                        else{
                            if(input[indexOfInputDecimalPoint + patternRearPart_length ] == '9'){
                                var  lastChar = input.Substring(indexOfInputDecimalPoint + patternRearPart_length - 1, 1);
                                //Console.WriteLine("{0}", lastChar);
                                var lastInt = Int16.Parse(lastChar) + 1;
                                //Console.WriteLine("{0}", lastInt);
                                var str = lastInt.ToString();
                                //Console.WriteLine("string {0}", str);
                                char[] c = str.ToCharArray(); 
                                StringBuilder sb = new StringBuilder(input);
                                sb[indexOfInputDecimalPoint + patternRearPart_length] = '0';
                                sb[indexOfInputDecimalPoint + patternRearPart_length - 1] = c[0];
                                input = sb.ToString();
                                input = input.Substring(0, inputKeepPart);
                                input = input.Insert(0, "(");
                                input = input.Insert(indexOfInputDecimalPoint + 1, ")");
                            }
                            else{
                                //kí tự cần làm tròn của input cuối cùng
                                var  lastChar = input.Substring(indexOfInputDecimalPoint + patternRearPart_length, 1);
                                //Console.WriteLine("{0}", lastChar);
                                var lastInt = Int16.Parse(lastChar) + 1;
                                //Console.WriteLine("{0}", lastInt);
                                var str = lastInt.ToString();
                                //Console.WriteLine("string {0}", str);
                                char[] c = str.ToCharArray(); 
                                StringBuilder sb = new StringBuilder(input);
                                sb[indexOfInputDecimalPoint + patternRearPart_length] = c[0]; 
                                input = sb.ToString();
                                input = input.Substring(0, inputKeepPart);
                                input = input.Insert(0, "(");
                                input = input.Insert(indexOfInputDecimalPoint + 1, ")");
                            }
                        }    
                }       
            }
            return input;
        }
    
        static void Main(string[] args)
        {
            string input = "75674.73789621"; 
            Console.WriteLine("String Input: {0}", input);
            var Zero_Placeholder = formatString("{0:00.00000}", input); //output is : 75674.73790
            var Digit_Placeholder = formatString("{0:(#).#####}", input); //output is : (75674).73790
            var Decimal_Point = formatString("{0:0.0000}", input); //output is : 75674.7379
            var Thousand_Separator = formatString("{0:0,0}", input); //output is : 75,675
            Console.WriteLine("Format using Zero Placeholder: {0}", Zero_Placeholder);
            Console.WriteLine("Format using Digit Placeholder: {0}", Digit_Placeholder);
            Console.WriteLine("Format using Decimal Point: {0}", Decimal_Point);
            Console.WriteLine("Format using Thousand Separator: {0}", Thousand_Separator);
        }
    }
}
